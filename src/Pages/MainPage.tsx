import Story from '../Core/Story';
import UniverseStory from '../DataTypes/UniverseStory';
import Page from '../Core/Page';
import h from '../Functionality/React';
import { GET, POST, setCookie } from '../Functionality/Retrieve';

import "./MainPage.scss";


export default class MainPage extends Page {
	type = "MainPage";

	referencer: HTMLElement = null;

	async _Begin() {
		this.parent.universe.categorize();

		let list = [];

		let temp = this.element.appendChild(
			(<div class="list"></div>)

		);
		for (let key in this.parent.universe.story) {
			let cur = this.parent.universe.story[key];

			let a = temp.appendChild((
				<div data-id={cur.id} className="story">
					<div className="place">
						{this.parent.universe.data[cur.place].name}
					</div>
					<div className="time">
						{cur.time}
					</div>
					<div className="text">
					</div>
				</div>
			));

			this.parent.universe.story[key].returnWithReferences(a.querySelector(".text"), this.parent.universe, this.parent)
		}

		this.referencer = this.element.appendChild(
			(<div class="referencer"></div>)

		);
		for (let i = 0; i < list.length; i++) {
			this.referencer.appendChild(<div><span style='font-size:30px;'>{list[i].name}</span><br />{list[i].description}<br /></div >);
		}

	}

	_End() {

	}
}