import Story from '../Core/Story';
import Page from '../Core/Page';
import h from '../Functionality/React';
import { GET, POST, setCookie } from '../Functionality/Retrieve';

import "./CustomPage.scss";
import DataPage from './DataPage';


export default class CustomPage extends DataPage {
	type = "CustomPage";

	dataType = "";

	constructor(dataType) {
		super();
		this.dataType = dataType;
	}

	/*async _Begin() {

		this.element.appendChild((
			<div>
				<b>Custom</b>
			</div>
		));

	}*/

	_End() {

	}
}