import Story from '../Core/Story';
import Page from '../Core/Page';
import h from '../Functionality/React';
import { GET, POST, setCookie } from '../Functionality/Retrieve';


export default class SelectUniversePage extends Page {
	type = "SelectUniversePage";

	async _Begin() {
		if (Object.keys(this.parent.universeList).length == 0) {
			await this.parent.loadUniverseList();
		}

		let elements = [];
		for (let key in this.parent.universeList) {
			elements.push(<div className="universe">{this.parent.universeList[key]["name"]}</div>);
		}
		let i = 0;
		for (let key in this.parent.universeList) {
			elements[i].addEventListener("click", () => this.parent.selectUniverse(key));
			i++;
		}

		this.element.appendChild((
			<div>
				{elements}
			</div>
		));

	}

	_End() {

	}
}