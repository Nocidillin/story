import Router from '../Functionality/Router';
import Page from '../Core/Page';
import MainPage from './MainPage';
import h from '../Functionality/React';
import { GET, POST, setCookie } from '../Functionality/Retrieve';

import "./LoginPage.scss";
import SelectUniversePage from './SelectUniversePage';


export default class LoginPage extends Page {
	type = "LoginPage";

	listener = null;

	url = null;

	constructor(url) {
		super();
		this.url = url;
	}

	async Submit() {
		let username = (document.getElementById("username") as HTMLInputElement).value;
		let pass = (document.getElementById("password") as HTMLInputElement).value;
		let result = await POST<any>("https://katskit.com/story/login.php", { "username": username, "password": pass });
		this.Test(result)
	}

	Test(result) {
		if (result == 200) {
			setCookie("loggedIn", true, 31);
			this.parent.loggedIn = true;
			if (this.url != null) {
				Router.router(this.url);
			} else {
				this.parent.setPage(new SelectUniversePage());
			}
		} else if (result == 403) {
			window.history.pushState({}, "login", "/login");
		} else if (result == 404) {
			window.history.pushState({}, "login", "/login");
		} else {
			window.history.pushState({}, "login", "/login");
		}
	}

	_Begin() {
		this.element.appendChild((
			<div>
				<h1>Universes</h1>
				<br />
				<form action="javascript:void(0);">
					<h3>Username</h3>
					<input autocomplete="username" id="username"></input>
					<h3>Password</h3>
					<input autocomplete="current-password" id="password" type="password"></input>
					<br />
					<br />
					<button id="login">Log In</button>
				</form>
			</div >
		));

		this.listener = document.querySelector<HTMLElement>("#login").addEventListener("click", () => this.Submit());
	}

	_End() {
		document.querySelector<HTMLElement>("#login").removeEventListener("click", this.listener);
	}
}