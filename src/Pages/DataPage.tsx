import Story from '../Core/Story';
import Router from '../Functionality/Router';
import Universe from '../Core/Universe';
import UniverseContent from '../DataTypes/UniverseContent';
import Page from '../Core/Page';
import Timeline from '../Components/TimelineVertical';
import h from '../Functionality/React';
import { GET, POST, setCookie } from '../Functionality/Retrieve';



export default class DataPage extends Page {
	type = "DataPage";

	itemIndex: number = null;
	dataType: string = null;

	list: { [key: string]: UniverseContent } = {};

	timeline: Timeline = null;

	selectItem(index: number) {

		Router.setAddress(["#universe", this.dataType + "s", this.getKeyFromId(index)]);
		this.itemMenu.style.display = "block";
		this.itemContainer.style.display = "none";
		this.newMenu.style.display = "none";

		this.itemIndex = index;

		let selectedItem = this.parent.universe.data[index];

		this.itemMenu.querySelector(".name").value = selectedItem.name;
		this.itemMenu.querySelector(".tags").value = selectedItem.tags.join(", ");
		this.itemMenu.querySelector(".description").value = selectedItem.description;

		let types: { [key: string]: UniverseContent[] } = {};
		console.log(selectedItem.references);
		for (let i = 0; i < selectedItem.references.length; i++) {
			let reference = selectedItem.references[i];
			if (!types.hasOwnProperty(reference.type)) {
				types[reference.type] = [];
			}
			types[reference.type].push(reference);
		}
		let referencesElement = this.itemMenu.querySelector(".references")
		referencesElement.innerHTML = "";
		for (let key in types) {
			let names = []
			for (let i = 0; i < types[key].length; i++) {
				names.push(types[key][i].name);
			}
			referencesElement.innerHTML += "<b>" + key + "</b> " + names.join(", ") + "</br>";
		}

		this.timeline = new Timeline(
			this.parent,
			selectedItem.tags.concat(selectedItem.type),
			selectedItem.references,
			this.itemMenu.querySelector(".Timeline"));
	}

	back() {
		let url = Router.retrieveURL(window.location.href);
		window.history.pushState({}, "", Router.source + url[0] + "/" + this.dataType + "s");
		this.itemMenu.style.display = "none";
		this.itemContainer.style.display = "block";
		this.newMenu.style.display = "none";
	}

	getDataOfType(type: string) {
		return this.parent.universe.findType(type);
	}

	getTag(tag: string) {
		return this.parent.universe.findTag(tag);
	}
	itemMenu: any = null;
	itemContainer: any = null;
	newMenu: any = null;

	getFancyDataItemList(list) {
		let listOfUniverses: { [key: string]: UniverseContent } = {};
		for (let key in list) {
			this.addKey(list[key], listOfUniverses);
		}
		return listOfUniverses;
	}

	addKey(item, result) {
		let name = Router.Simplify(item["name"]);
		let index = 2;
		while (result.hasOwnProperty(name)) {
			name = Router.Simplify(item["name"]) + "-" + index;
			index++;
		}
		result[name] = item;
	}

	getKeyFromId(id) {

		let listOfDataItems = this.getFancyDataItemList(this.getDataOfType(this.dataType));

		let universeKey = "";
		for (let key in listOfDataItems) {
			if (listOfDataItems[key].id == id) {
				universeKey = key;
				break;
			}
		}
		return universeKey;
	}

	createNew() {
		Router.setAddress(["#universe", this.dataType + "s", "new"]);
		this.newMenu.style.display = "block";
		this.itemMenu.style.display = "none";
		this.itemContainer.style.display = "none";
	}

	async sendNew() {
		let result = await POST<any>("https://katskit.com/story/send.php", {
			"name": this.newMenu.querySelector(".name").value,
			"tags": this.newMenu.querySelector(".tags").value.replace(/, /g, ",").split(","),
			"description": this.newMenu.querySelector(".description").value,
			"type": this.dataType,
			"timestamp": 0,
			"universe": this.parent.activeUniverse
		})

		this.parent.universe.insert(result);
		this.addKey(result, this.list);
		this.selectItem(result["id"]);
	}

	async sendUpdate() {
		//console.log("sending new update");
		let result = await POST<any>("https://katskit.com/story/send.php", {
			"name": this.itemMenu.querySelector(".name").value,
			"tags": this.itemMenu.querySelector(".tags").value.replace(/, /g, ",").split(","),
			"description": this.itemMenu.querySelector(".description").value,
			"type": this.dataType,
			"timestamp": this.parent.universe.data[this.itemIndex].timestamp.time,
			"universe": this.parent.activeUniverse,
			"update": true,
			"id": this.itemIndex
		})

		this.parent.universe.update(this.parent.universe.data[this.itemIndex], result);
		this.addKey(result, this.list);

		let list = this.getDataOfType(this.dataType);
		this.rebuildList(list);
		this.back();
	}

	rebuildList(list) {
		if (this.itemContainer != null) {
			this.itemContainer.remove();
		}

		let outputList = [];

		for (let key in list) {
			outputList.push(<div className="item">{list[key].name}<div className="description">{list[key].description}</div></div>);
			outputList[outputList.length - 1].addEventListener("click", () => this.selectItem(list[key].id));
		}

		outputList.push(<div className="item">New<div className="description">Create a new <b>{this.dataType}</b></div></div>);
		outputList[outputList.length - 1].addEventListener("click", () => this.createNew());

		this.itemContainer = this.element.appendChild((
			<div class="itemListContainer">
				{outputList}
			</div>
		));

	}

	async _Begin() {
		let list = this.getDataOfType(this.dataType);
		this.list = this.getFancyDataItemList(list);
		let url = Router.retrieveURL(window.location.href);

		this.rebuildList(list);

		this.itemMenu = this.element.appendChild((
			<div id="item" style="display: none;">
				<div className="back">Back</div>
				<input className="name edit"></input>
				<input className="tags edit"></input>
				<textarea className="description edit"></textarea>
				<button className="update">Update</button>
				<div className="references"></div>

				<div class="Timeline">
				</div>

			</div>
		));

		this.newMenu = this.element.appendChild((
			<div id="item" style="display: none;">
				<div className="back">Back</div>
				<input className="name edit">Select a name</input>
				<input className="tags edit">Define some tags</input>
				<textarea className="description edit">Description</textarea>
				<button className="finish">Finish</button>
			</div>
		));


		this.newMenu.querySelector(".finish").addEventListener("click", () => this.sendNew());

		if (url.length > 2) {
			if (url[1] == this.dataType + "s") {
				if (url[2] == "new") {
					this.createNew();
				} else {
					this.selectItem(this.list[Router.Simplify(url[2])].id);
				}
			}
		}

		this.itemMenu.querySelector(".back").addEventListener("click", () => this.back());
		this.itemMenu.querySelector(".update").addEventListener("click", () => this.sendUpdate());

		this.newMenu.querySelector(".back").addEventListener("click", () => this.back());
	}

	_End() {

	}
}