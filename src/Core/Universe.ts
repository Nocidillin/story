import UniverseContent from '../DataTypes/UniverseContent';
import UniverseStory from '../DataTypes/UniverseStory';
import UniversePicture from '../DataTypes/UniversePicture';
import UniverseTimestamp from '../DataTypes/UniverseTimestamp';

export default class Universe {

	categorized: boolean = false;

	universeID: number = null;

	data: { [key: string]: UniverseContent } = {};
	pictures: { [key: string]: UniversePicture[] } = {};

	story: { [key: string]: UniverseStory } = {};

	taggedData: { [key: string]: UniverseContent[] } = {};
	typedData: { [key: string]: UniverseContent[] } = {};

	dictionaryData: { [key: string]: UniverseContent[] } = {};

	modules: {};

	public import(value: any) {
		UniverseTimestamp.import(JSON.parse(value.date_format));
		for (let key in value.data) {
			this.data[key] = new UniverseContent(value.data[key]);
		}
		for (let key in value.story) {
			this.story[key] = new UniverseStory(value.data[key]);
		}
		for (let key in value.pictures) {
			let pictureData = value.pictures[key];
			let picture = new UniversePicture(pictureData)
			this.data[pictureData.data_id].pictures.push(picture);
		}
		for (let key in value.references) {
			let referencesData = value.references[key];

			this.data[referencesData.to_data_id].references.push(this.data[referencesData.from_data_id]);
			this.data[referencesData.from_data_id].references.push(this.data[referencesData.to_data_id]);
		}

		this.modules = JSON.parse(value.module);
		this.categorize();
	}

	categorize() {
		//console.log(this);
		if (!this.categorized) {
			for (let key in this.data) {
				this.categorizeItem(key)
			}
			this.categorized = true;
		}
	}

	public categorizeItem(key) {
		let tags = this.data[key].tags;
		for (let i = 0; i < tags.length; i++) {
			if (!this.taggedData.hasOwnProperty(tags[i])) {
				this.taggedData[tags[i]] = [];
			}
			this.taggedData[tags[i]].push(this.data[key]);
		}

		if (!this.typedData.hasOwnProperty(this.data[key].type)) {
			this.typedData[this.data[key].type] = [];
		}
		this.typedData[this.data[key].type].push(this.data[key]);

		if (!this.dictionaryData.hasOwnProperty(key)) {
			this.dictionaryData[this.data[key].name.toLowerCase()] = [];
		}
		this.dictionaryData[this.data[key].name.toLowerCase()].push(this.data[key]);
	}

	public insert(item) {
		this.data[item.id] = new UniverseContent(item);
		this.categorizeItem(item.id);
	}

	removeItem(key, item, list) {
		list[key].splice(list[key].indexOf(item), 1);

	}

	public update(oldData, item) {
		this.removeItem(oldData.name.toLowerCase(), oldData, this.dictionaryData);
		this.removeItem(oldData.type, oldData, this.typedData);

		for (let i = 0; i < oldData.tags.length; i++) {
			this.removeItem(oldData.tags[i], oldData, this.taggedData);
		}

		this.insert(item);
		this.data = this.order(this.data);
		this.typedData = this.order(this.typedData);
		this.dictionaryData = this.order(this.dictionaryData);
		this.typedData[item.type].sort(function (a, b) { return a.id - b.id });

	}

	public order(list) {
		let ordered = {};
		let unordered = list;
		Object.keys(unordered).sort().forEach(function (key) {
			ordered[key] = unordered[key];
		});
		return ordered;
	}

	public findTag(tag: string): UniverseContent[] {
		if (!this.categorized) {
			this.categorize();
		}
		if (this.taggedData.hasOwnProperty(tag)) {
			return this.taggedData[tag];
		} else {
			return null;
		}
	}

	public findType(type: string): UniverseContent[] {
		if (!this.categorized) {
			this.categorize();
		}
		if (this.typedData.hasOwnProperty(type)) {
			return this.typedData[type];
		} else {
			return null;
		}
	}
}