import Story from './Story';

export default class Page {
	active: boolean = false;
	element: HTMLElement = null;
	parent: Story = null;
	type: string = null;

	Begin() {
		if (!this.active) {
			this.active = true;
			this.element = document.createElement("div");
			this.element.className = "page";
			this.element.id = this.type;

			document.querySelector("body").appendChild(this.element);
			this._Begin();
		}
	}

	End() {
		if (this.active) {
			this._End();
			this.element.parentElement.removeChild(this.element);
		}
	}

	_Begin() { }
	_End() { }
}