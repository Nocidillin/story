import "./Core.scss";
import "./Header.scss";

import Page from './Page';
import Universe from './Universe';
import { getCookie, POST } from '../Functionality/Retrieve';
import Router from "../Functionality/Router";
import CustomPage from '../Pages/CustomPage';


export default class Story {
	activePage: Page = null;
	loggedIn: boolean = false;

	activeUniverse: number = null;

	universe: Universe = null;


	universeList: { [key: number]: object } = {};

	async Begin() {
		this.loggedIn = await this.checkLoginStatus();
		Router.Begin(this);
		this.setHeaderButtons();
		Router.router(Router.retrieveURL(document.location.href));
	}

	protected setHeaderButtons() {
		document.querySelector("header").innerHTML = "";
		console.log(this.headerButtons);
		for (let i = 0; i < this.headerButtons.length; i++) {
			let button = document.createElement("div");
			button.className = "button " + this.headerButtons[i]["icon"];
			button.innerText = this.headerButtons[i]["text"];
			document.querySelector("header").appendChild(button);
			button.onclick = () => Router.router(this.headerButtons[i]["url"]);
		}
	}

	async enableHeader() {
		document.querySelector("header").style.display = "";
	}

	async disableHeader() {
		document.querySelector("header").style.display = "none";
	}

	async loadUniverseList() {
		let value = await POST<any>("https://katskit.com/story/retrieve.php", { "type": "list" });
		if (value.hasOwnProperty("error") != true) {
			this.universeList = value;
			return true;
		} else {
			//console.log("error:", value["error"]);
			return false;
		}
	}

	async checkLoginStatus(): Promise<boolean> {
		if (await this.loadUniverseList()) {
			return getCookie("loggedIn") == "true";
		} else {
			return false;
		}
	}

	async importCustomModule(customModule: any, key: string) {
		Router.pages[key + "s"] = function () { return new CustomPage(key); };
		this.headerButtons.splice(this.headerButtons.length - 1, 0, { icon: customModule.icon, text: customModule.fancy, url: ["#universe", key + "s"], custom: true });
		console.log(this.headerButtons);
	}


	async loadUniverse(id) {
		if (this.universe == null) {
			for (let i = 0; i < this.headerButtons.length; i++) {
				if (this.headerButtons[i].custom) {
					this.headerButtons.splice(i, 1);
					i--;
				}
			}
			let value = await POST<any>("https://katskit.com/story/retrieve.php", { "type": "universe", "universe": id });
			if (value.hasOwnProperty("error") != true) {
				this.universe = new Universe();
				this.universe.import(value);
				for (let key in this.universe.modules) {
					this.importCustomModule(this.universe.modules[key], key);
				}
				this.setHeaderButtons();
			} else {
				console.log("error:", value["error"]);
				return false;
			}
		}
		return true;
	}

	async selectUniverse(id) {
		this.activeUniverse = id;
		await this.loadUniverse(this.activeUniverse);
		Router.router([Router.getKeyFromId(Router.getUniverseList(), id), "main"]);
	}

	startLoading() {

	}

	stopLoading() {

	}

	setPage(newPage: Page) {
		newPage.parent = this;
		if (this.activePage != null) {
			this.activePage.End();
		}
		this.activePage = newPage;
		this.activePage.Begin();
	}

	headerButtons = [
		{ icon: "main", text: "Home", url: ["#universe", "main"], custom: false },
		{ icon: "characters", text: "Characters", url: ["#universe", "characters"], custom: false },
		{ icon: "events", text: "Events", url: ["#universe", "events"], custom: false },
		{ icon: "locations", text: "Locations", url: ["#universe", "locations"], custom: false },
		{ icon: "config", text: "Config", url: ["#universe", "config"], custom: false }
	];

}