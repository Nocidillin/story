
import h from '../Functionality/React';

import "./KatTextEditor.scss";

export default class KatTextEditor {

	editorElement: HTMLElement = null;
	text: string = null;

	constructor(div: HTMLElement, text: string) {
		this.text = text;
		this.text = this.text.replace(/\n/g, "</br>");
		this.text = this.text.replace(/\t/g, "&#9;");
		this.editorElement = div.appendChild((<div contentEditable="true" ></div>));
		this.editorElement.className = "KatTextEditor";
		this.editorElement.innerHTML = this.text;
		this.editorElement.onchange = () => this.editorOnChange();
		this.editorElement.onkeydown = (event: KeyboardEvent) => this.editorKeyDown(event)
		this.editorElement.onkeyup = (event: KeyboardEvent) => this.editorKeyUp(event);
	}

	setCaretPosition(x) {
		let range = document.createRange();
		let sel = window.getSelection();
		let y = 0;
		for (let i = 0; i < this.editorElement.childNodes.length; i++) {
			range.setStart(this.editorElement.childNodes[i], x - y);
			let tempRange = Range.toString().length
			console.log(tempRange);
			if (y + tempRange < x) {
				y = y + tempRange;
			} else {
				range.setStart(this.editorElement.childNodes[i], x - y);
				break;
			}
		}
		range.collapse(true);
		sel.removeAllRanges();
		sel.addRange(range);
	}

	getCaretPosition() {
		let caretOffset = 0;
		if (typeof window.getSelection !== "undefined") {
			let range = window.getSelection().getRangeAt(0);
			let selected = range.toString().length;
			let preCaretRange = range.cloneRange();
			preCaretRange.selectNodeContents(this.editorElement);
			preCaretRange.setEnd(range.endContainer, range.endOffset);
			caretOffset = preCaretRange.toString().length;
		}
		return caretOffset;
	}

	private setText(text: string) {
		this.text = text;
		this.editorElement.innerHTML = text;
	}

	private editorKeyDown(event: KeyboardEvent) {
		let position = this.getCaretPosition();
		if (event.keyCode == 9) { // tab
			this.setText(this.text.substr(0, position) + "<div class='tab'></div>" + this.text.substr(position));
			this.setCaretPosition(position + 1);
			return false;
		}
		return true;
	}

	private editorKeyUp(event: KeyboardEvent) {
		//let position = this.getCaretPosition();
		//console.log("position", position);

		/*	if (event.keyCode == 8) { // delete
				if (this.text.substr(position, 1) == ">"){
	
				}
				this.setText(this.text.substr(0, position - 1) + this.text.substr(position));
				this.setCaretPosition(position + 1);
				return false;
			}
			*/
		if (event.keyCode == 9) { // tab
			return false;
		}
		return true;
	}

	private editorOnChange() {
		let text = this.editorElement.innerHTML;
	}
}