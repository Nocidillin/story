
import Story from '../Core/Story';
import UniverseContent from '../DataTypes/UniverseContent';
import UniverseTimestamp from '../DataTypes/UniverseTimestamp';
import h from '../Functionality/React';

const vertical = true;

import "./TimelineVertical.scss";

export default class Timeline {
	searchData: string[] = [];
	references: UniverseContent[] = [];
	parent: Story = null;
	timelineElement: HTMLElement = null;
	tableElement: HTMLElement = null;

	constructor(story: Story, data: string[], references: UniverseContent[], element: HTMLElement) {
		this.searchData = data;
		this.parent = story;
		this.timelineElement = element;
		this.references = references;
		this.buildTimeline();
	}

	list: {
		[key: number]: { //year
			[key: number]: { //month
				[key: number]: {  // day
					[key: number]: UniverseContent
				}
			}
		}
	} = {};

	searchTag(key) {
		if (this.parent.universe.taggedData.hasOwnProperty(key)) {
			for (let i = 0; i < this.parent.universe.taggedData[key].length; i++) {
				this.addListElement(this.parent.universe.taggedData[key][i]);
			}
		}
	}

	searchType(key) {
		if (this.parent.universe.typedData.hasOwnProperty(key)) {
			for (let i = 0; i < this.parent.universe.typedData[key].length; i++) {
				this.addListElement(this.parent.universe.typedData[key][i]);
			}
		}
	}


	public order(list) {
		let ordered = {};
		let unordered = list;
		Object.keys(unordered).sort(
			function (a, b) {
				let x = parseInt(a);
				let y = parseInt(b);
				return x - y;
			}).forEach(function (key) {
				ordered[key] = unordered[key];
			});
		return ordered;
	}


	addListElement(item: UniverseContent) {

		let curTimestamp = item.timestamp;
		let year = curTimestamp.getYear();
		let month = curTimestamp.getMonth();
		let day = curTimestamp.getDay();

		if (year != null) {
			if (!this.list.hasOwnProperty(year)) {
				this.list[year] = {};
			}
			if (!this.list[year].hasOwnProperty(month)) {
				this.list[year][month] = {};
			}
			if (!this.list[year][month].hasOwnProperty(day)) {
				this.list[year][month][day] = {};
			}
			this.list[year][month][day][item.id] = item;
		}
	}

	createElement(items: { [key: number]: UniverseContent }, year, type) {
		let cur = this.tableElement.appendChild((<tr></tr>));

		let elementType = "div";

		let left = cur.appendChild((<td class={"item left" + (type ? " date" : "")}></td>))
		cur.appendChild((<td class="line"></td>))
		let right = cur.appendChild((<td class={"item right" + (!type ? " date" : "")}></td>))

		let time = right;
		let data = left;
		if (type) {
			time = left;
			data = right;
		}
		let count = 0;
		let lastKey = "";
		let imagesList = [];
		//console.log("pictures", this.parent.universe.pictures);
		for (let key in items) {
			count++;
			if (items[key].pictures.length != 0) {
				imagesList.push(items[key].pictures)
			}
			lastKey = key;
		}
		if (imagesList.length > 0) {
			console.log()
			data.classList.add("biggy");
			let images = data.appendChild((<div className="images"></div>))
			for (let i = 0; i < Math.min(imagesList.length, 3); i++) {
				let image = images.appendChild((<div></div>))
				image.style.backgroundImage = "url(\"/assets/custom/" + imagesList[i][0].source + "\")";
				for (let key in items) {
					//console.log(items[key], imagesList[i][0])
					if (items[key].id == imagesList[i][0].data_id) {
						let name = "<b>" + items[key].name + "</b>";
						if (items[key].type == "character") {
							name += " born";
						}
						image.innerHTML += "<" + elementType + ">" + name + "</" + elementType + ">";
					}
				}
			}
		}


		for (let key in items) {
			let name = "<b>" + items[key].name + "</b>";
			if (items[key].type == "character") {
				name += " born";
			}
			data.innerHTML += "<" + elementType + ">" + name + "</" + elementType + ">";
		}

		if (count == 1) {
			time.innerHTML += "<" + elementType + ">" + items[lastKey].timestamp.getDisplayTime() + "</" + elementType + ">";
		} else {
			time.innerHTML += "<" + elementType + ">" + year + "</" + elementType + ">";
		}

	}


	first = null;
	length = null;
	end = null;

	getScale() {
		this.first = null;
		this.end = null;
		for (let key in this.list) {
			let num = parseInt(key);
			if (this.first == null) {
				this.first = num;
			}
			if (this.end == null) {
				this.end = num;
			}
			if (this.end < num) {
				this.end = num;
			}
			if (this.first > num) {
				this.first = num;
			}
		}
		this.length = this.end - this.first;
	}

	buildTimeline() {
		this.timelineElement.innerHTML = "";
		this.list = {};
		for (let i = 0; i < this.searchData.length; i++) {
			let item = this.searchData[i];
			this.searchTag(item);
			this.searchType(item);
		}
		for (let i = 0; i < this.references.length; i++) {
			this.addListElement(this.references[i]);
		}
		this.list = this.order(this.list);
		this.tableElement = this.timelineElement.appendChild((<table cellspacing="0"></table>));

		/*	this.timelineElement.appendChild(
				<tr></tr>
			)*/
		/*
		this.timelineElement.appendChild(
			<div className="top">

			</div>
		);
		this.timelineElement.appendChild(
			<div className="line">

			</div>
		);
		this.timelineElement.appendChild(
			<div className="bottom">

			</div>
		);*/

		//this.getScale();

		let flip = true;
		for (let year in this.list) {
			let list = [];
			for (let month in this.list[year]) {
				for (let day in this.list[year][month]) {
					for (let id in this.list[year][month][day]) {
						let item = this.list[year][month][day][id];
						list.push(item);
					}
				}
			}
			//console.log(list);
			this.createElement(list, year, flip)
			flip = !flip;
		}
	}

	destroy() {
		this.timelineElement.innerHTML = "";
	}
}

