
import Story from '../Core/Story';
import UniverseContent from '../DataTypes/UniverseContent';
import UniverseTimestamp from '../DataTypes/UniverseTimestamp';
import h from '../Functionality/React';

const vertical = true;

import "./TimelineHorizontal.scss";

export default class Timeline {
	searchData: string[] = [];
	parent: Story = null;
	timelineElement: HTMLElement = null;

	constructor(story: Story, data: string[], element: HTMLElement) {
		this.searchData = data;
		this.parent = story;
		this.timelineElement = element;
		this.buildTimeline();
	}

	list: {
		[key: number]: { //year
			[key: number]: { //month
				[key: number]: {  // day
					[key: number]: UniverseContent
				}
			}
		}
	} = {};

	searchTag(key) {
		if (this.parent.universe.taggedData.hasOwnProperty(key)) {
			for (let i = 0; i < this.parent.universe.taggedData[key].length; i++) {
				this.addListElement(this.parent.universe.taggedData[key][i]);
			}
		}
	}

	searchType(key) {
		if (this.parent.universe.typedData.hasOwnProperty(key)) {
			for (let i = 0; i < this.parent.universe.typedData[key].length; i++) {
				this.addListElement(this.parent.universe.typedData[key][i]);
			}
		}
	}

	public order(list) {
		let ordered = {};
		let unordered = list;
		Object.keys(unordered).sort(
			function (a, b) {
				let x = parseInt(a);
				let y = parseInt(b);
				return x - y;
			}).forEach(function (key) {
				ordered[key] = unordered[key];
			});
		return ordered;
	}


	addListElement(item: UniverseContent) {

		let curTimestamp = item.timestamp;
		let year = curTimestamp.getYear();
		let month = curTimestamp.getMonth();
		let day = curTimestamp.getDay();

		if (year != null) {
			if (!this.list.hasOwnProperty(year)) {
				this.list[year] = {};
			}
			if (!this.list[year].hasOwnProperty(month)) {
				this.list[year][month] = {};
			}
			if (!this.list[year][month].hasOwnProperty(day)) {
				this.list[year][month][day] = {};
			}
			this.list[year][month][day][item.id] = item;
		}
	}

	createElement(items: { [key: number]: UniverseContent }, year, element, oppositeElement) {
		let cur = element.appendChild((<td class="item"></td>));

		let elementType = "p";
		if (vertical) {
			elementType = "td";
		}

		let count = 0;
		let lastKey = "";
		for (let key in items) {
			count++;
			lastKey = key;
		}
		if (!vertical) {
			if (count == 1) {
				cur.innerHTML += "<" + elementType + ">" + new UniverseTimestamp(items[lastKey].timestamp, items[lastKey].timestamp_state).getDisplayTime() + "</" + elementType + ">";
			} else {
				cur.innerHTML += "<" + elementType + ">" + year + "</" + elementType + ">";
			}
		}

		for (let key in items) {
			let name = "<b>" + items[key].name + "</b>";
			if (items[key].type == "character") {
				name += " born";
			}
			cur.innerHTML += "<" + elementType + ">" + name + "</" + elementType + ">";
		}
		if (!vertical) {
			cur.style.left = ((parseInt(year) - parseInt(this.first)) / this.length * 80 + 10) + "%";
		} else {
			let timestamp = oppositeElement.appendChild((<td class="item date"></td>));

			if (count == 1) {
				timestamp.innerHTML += "<" + elementType + ">" + new UniverseTimestamp(items[lastKey].timestamp, items[lastKey].timestamp_state).getDisplayTime() + "</" + elementType + ">";
			} else {
				timestamp.innerHTML += "<" + elementType + ">" + year + "</" + elementType + ">";
			}
		}
	}


	first = null;
	length = null;
	end = null;

	getScale() {
		this.first = null;
		this.end = null;
		for (let key in this.list) {
			let num = parseInt(key);
			if (this.first == null) {
				this.first = num;
			}
			if (this.end == null) {
				this.end = num;
			}
			if (this.end < num) {
				this.end = num;
			}
			if (this.first > num) {
				this.first = num;
			}
		}
		this.length = this.end - this.first;
	}

	buildTimeline() {
		this.timelineElement.innerHTML = "";
		this.list = {};
		for (let i = 0; i < this.searchData.length; i++) {
			let item = this.searchData[i];
			this.searchTag(item);
			this.searchType(item);
		}
		this.list = this.order(this.list);
		console.log(this.list);

		this.timelineElement.appendChild(
			<div className="top">

			</div>
		);
		this.timelineElement.appendChild(
			<div className="line">

			</div>
		);
		this.timelineElement.appendChild(
			<div className="bottom">

			</div>
		);

		this.getScale();

		let flip = true;
		for (let year in this.list) {
			let list = [];
			for (let month in this.list[year]) {
				for (let day in this.list[year][month]) {
					for (let data_id in this.list[year][month][day]) {
						let item = this.list[year][month][day][data_id];
						list.push(item);
					}
				}
			}
			this.createElement(list, year, (flip ? this.timelineElement.querySelector(".top") : this.timelineElement.querySelector(".bottom")), (!flip ? this.timelineElement.querySelector(".top") : this.timelineElement.querySelector(".bottom")))
			flip = !flip;
		}
	}

	destroy() {
		this.timelineElement.innerHTML = "";
	}
}

