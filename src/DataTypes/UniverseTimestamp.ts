import { deflateSync } from "zlib";

export default class UniverseTimestamp {
	time: number = null;
	type: string = null;

	static starting_year: number = 1970;
	static starting_month: number = 0;
	static starting_day: number = 0;

	static month_count: number = 0;

	static import(value) {
		this.starting_year = value.s_year;
		this.starting_month = value.s_month;
		this.starting_day = value.s_day;

		this.months_format = value.months;

		for (let temp_key in UniverseTimestamp.months_format) {
			this.month_count++;
		}
	}

	static months_format: { [key: string]: number } = {
		January: 31,
		February: 28.25,
		March: 31,
		April: 30,
		May: 31,
		July: 30,
		June: 31,
		August: 31,
		September: 30,
		October: 31,
		November: 30,
		December: 31
	};

	constructor(timestamp = null, type: string) {
		this.time = parseInt(timestamp);
		this.type = type;
	}


	public getYear() {
		return (this.getTime().year);
	}
	public getDay() {
		return (this.getTime().day);
	}
	public getMonth() {
		return (this.getTime().month);
	}


	public getDisplayTime() {
		let time = this.getTime();

		if (this.type == "DayKnown") {
			return this.getMonthName(time.month) + " " + time.day + ", " + time.year;
		} else if (this.type == "MonthKnown") {
			return this.getMonthName(time.month) + ", " + time.year;
		} else if (this.type == "YearKnown") {
			return time.year;
		} else if (this.type == "Unknown") {
			return null;
		}
		return null;
	};

	private getMonthName(month) {
		let count = 0;
		for (let temp_key in UniverseTimestamp.months_format) {
			if (count == month) {
				return temp_key;
			}
			count++;
		}
		return month;
	}

	private getDayCountFromMonthByKey(key): number {
		let count = 0;
		for (let temp_key in UniverseTimestamp.months_format) {
			if (count == key) {
				return UniverseTimestamp.months_format[temp_key];
			}
			count++;
		}
		return 0;
	}

	private getPositiveMonth(month) {
		while (month < 0) {
			month += UniverseTimestamp.month_count;
		}
		return month;
	}

	private getTime() {
		let curTimestamp: number = this.time;

		let year: number = UniverseTimestamp.starting_year;
		let month: number = UniverseTimestamp.starting_month;
		let day: number = UniverseTimestamp.starting_day;

		curTimestamp += day;

		if (curTimestamp >= 0) {
			while (curTimestamp > this.getDayCountFromMonthByKey(month)) {
				curTimestamp -= this.getDayCountFromMonthByKey(month);
				month++;
				if (month >= UniverseTimestamp.month_count) {
					month = 0;
					year++;
				}
			}
		} else {
			while (curTimestamp + this.getDayCountFromMonthByKey(this.getPositiveMonth(month - 1)) < this.getDayCountFromMonthByKey(this.getPositiveMonth(month - 1))) {
				curTimestamp += this.getDayCountFromMonthByKey(month);
				month--;
				if (month < 0) {
					month = UniverseTimestamp.month_count - 1;
					year--;
				}
			}
		}

		day = Math.floor(curTimestamp + 1);

		if (this.type == "DayKnown") {
			return { day, month, year };
		} else if (this.type == "MonthKnown") {
			return { day: null, month, year };
		} else if (this.type == "YearKnown") {
			return { day: null, month: null, year };
		} else if (this.type == "Unknown") {
			return { year: null, month: null, day: null };
		}
	}

}