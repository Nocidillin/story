import UniverseData from "./UniverseData";


export default class UniversePicture extends UniverseData {
	universe_id: number = null;
	primary_image: boolean = null;
	source: string = null;

	constructor(data: any) {
		super(data);
		this.import(data);
	}
}