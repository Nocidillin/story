import UniverseTimestamp from './UniverseTimestamp'

export default class UniverseData {
	id: number = null;

	constructor(data: any) {
		this.import(data);
	}


	public import(data: any) {
		const keys = Object.keys(data);
		for (const key of keys) {
			if (data.hasOwnProperty(key)) {
				if (key == "timestamp") {
					(this as any)[key] = new UniverseTimestamp(data[key], data[key + "_state"]);
				} else {
					(this as any)[key] = data[key];
				}
			}
		}
		this.postImport(data);
	}
	protected postImport(data: any) {
		return;
	}
}