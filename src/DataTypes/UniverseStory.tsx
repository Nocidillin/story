import UniverseTimestamp from "./UniverseTimestamp";
import Universe from "../Core/Universe";
import KatTextEditor from "../Components/KatTextEditor";
import Story from "../Core/Story";
import Router from "../Functionality/Router";
import h from "../Functionality/React";
import UniverseData from "./UniverseData";


export default class UniverseStory extends UniverseData {
	universe_id: number = null;
	story_order: number = null;
	time: UniverseTimestamp = null;
	place: number = null;
	text: string = null;

	words: string[] = [];

	compiled: boolean = false;
	editor: KatTextEditor = null;

	constructor(data: any) {
		super(data);
		this.import(data);
	}

	_compile() {
		if ((this.compiled == true) == false) {
			this.compiled = true;
			this.words = this.text.split(" ");
		}
	};

	returnWithReferences(textElement: HTMLElement, parent: Universe, storyParent: Story) {
		this._compile();
		let list = [];
		parent.categorize();
		this.editor = new KatTextEditor(textElement, this.text);
		let result = (<div></div>);
		for (let i = 0; i < this.words.length; i++) {
			let temp = "";
			let found = false;
			for (let depth = 0; depth < Math.min(6, this.words.length - i); depth++) {
				if (depth != 0) temp += " ";
				temp += this.words[i + depth].replace(".", "").replace(",", "").toLowerCase();
				if (parent.dictionaryData.hasOwnProperty(temp)) {
					let gay = Router.retrieveURL(window.location.href);
					let url = Router.source + gay[0] + "/" + parent.dictionaryData[temp][0].type + "s/" + parent.dictionaryData[temp][0].name;
					list.push(parent.dictionaryData[temp][0]);
					let tempResult = (<a class='reference'></a>);
					tempResult.href = url;
					for (let sept = 0; sept <= depth; sept++) {
						tempResult.innerText = this.words[i + sept] + " ";
						found = true;
					}
					i += depth;
					result.appendChild(tempResult);
					result.innerHTML += " ";
					break;
				}
			}
			if (found == false) {
				result.innerHTML += this.words[i] + " ";
			}
		}
		result.innerHTML = result.innerHTML.replace(/\n/g, "<br>");
		result.innerHTML = result.innerHTML.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		return [list];
	}
};