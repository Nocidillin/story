import UniverseData from "./UniverseData";
import UniversePicture from "./UniversePicture";
import UniverseTimestamp from "./UniverseTimestamp";


export default class UniverseContent extends UniverseData {
	name: string = "";
	type: string = "";
	tags: string[] = [];
	description: string = null;

	timestamp: UniverseTimestamp = null;

	pictures: UniversePicture[] = [];

	references: UniverseContent[] = [];

	constructor(data: any) {
		super(data);
		this.import(data);
	}

	public findTag(tag: string) {
		for (let i = 0; i < this.tags.length; i++) {
			if (this.tags[i] == tag) {
				return true;
			}
		}
		return false;
	}

	public postImport() {
		let list = this.name.split(" ");
		for (let i = 0; i < list.length; i++) {
			let simplified = list[i].toLowerCase();
			if (!this.findTag(simplified)) {
				this.tags.push(simplified);
			}
		}
	}
}