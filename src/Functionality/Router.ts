import Page from '../Core/Page';
import Story from '../Core/Story';

import LoginPage from '../Pages/LoginPage';
import MainPage from '../Pages/MainPage';
import SelectUniversePage from '../Pages/SelectUniversePage';
import LocationsPage from '../Pages/DataPages/LocationsPage';
import CharactersPage from '../Pages/DataPages/CharactersPage';
import EventsPage from '../Pages/DataPages/EventsPage';
import CustomPage from '../Pages/CustomPage';
import ConfigPage from '../Pages/ConfigPage';

export default class Router {


	static source = "http://localhost:8080/";

	static pages: { [key: string]: () => Page } = {
		"login": function () { return new LoginPage({}); },
		"main": function () { return new MainPage(); },
		"selectuniverse": function () { return new SelectUniversePage(); },
		"characters": function () { return new CharactersPage(); },
		"events": function () { return new EventsPage(); },
		"locations": function () { return new LocationsPage(); },
		"config": function () { return new ConfigPage(); }
	};

	static currentURL = [];

	static dataList = {};
	static universeList = {};

	static parent: Story = null;
	static compiled: boolean = false;

	static Begin(story: Story) {
		Router.universeList = Router.getFancyList(story.universeList);
		Router.compiled = true;
		Router.parent = story;
	}

	static getUniverseDataList() {
		if (Object.keys(Router.dataList).length === 0) {
			Router.dataList = Router.getFancyList(Router.parent.universe.data);
		}
		return Router.dataList;
	}
	static getUniverseList() {
		if (Object.keys(Router.universeList).length === 0) {
			Router.universeList = Router.getFancyList(Router.parent.universeList);
		}
		return Router.universeList;
	}

	static async setAddress(url) {
		if (url[0] == "#universe") {
			url[0] = this.getKeyFromId(Router.universeList, Router.parent.activeUniverse);
		}
		window.history.pushState({}, "", Router.source + url.join("/"));
	}

	static async router(url) {
		console.log("Routing user to ", url)
		if (Router.parent.loggedIn) {
			if (url[0] == "#universe") {
				url[0] = this.getKeyFromId(Router.universeList, Router.parent.activeUniverse);
			}
			if (Router.universeList.hasOwnProperty(url[0])) {
				window.history.pushState({}, "", Router.source + url.join("/"));
				Router.parent.activeUniverse = Router.universeList[url[0]][1];
				await Router.parent.loadUniverse(Router.parent.activeUniverse);
				Router.parent.enableHeader();
				Router.currentURL = url;
				Router.parent.setPage(this.findNewPage(url[1]));
			}
			else {
				console.log("Routing to Select Universe menu.");
				window.history.pushState({}, "", this.source + "selectuniverse");
				Router.parent.disableHeader();
				Router.currentURL = ["selectuniverse"];
				Router.parent.setPage(new SelectUniversePage());
			}
		} else {
			window.history.pushState({}, "", this.source + "login");
			Router.parent.disableHeader();
			Router.currentURL = url;
			Router.parent.setPage(new LoginPage(url));
		}
	}

	static Simplify(string) {
		let result = (string.toLowerCase().replace(/[ .]/g, ""))
		return result
	}

	static retrieveURL(string) {
		let returnString = string;
		returnString = returnString.replace("https://", "");
		returnString = returnString.replace("http://", "");
		returnString = returnString.substring(returnString.indexOf("/") + 1);
		return returnString.split("/");
	}

	static getKeyFromId(list, id) {

		let resultKey = "";
		for (let key in list) {
			if (list[key][1] == id) {
				resultKey = key;
				break;
			}
		}
		return resultKey;
	}

	static getFancyList(list) {
		let typeList = {};
		for (let key in list) {
			let name = this.Simplify(list[key]["name"]);
			let index = 2;
			while (typeList.hasOwnProperty(name)) {
				name = this.Simplify(list[key]["name"]) + "-" + index;
				index++;
			}
			typeList[name] = [list[key]["name"], parseInt(key)];
		}
		return typeList;
	}

	static cut(pageName) {
		return pageName.substring(0, pageName.length - 4).toLowerCase();
	}


	static findNewPage(page) {
		//console.log(page);
		return Router.pages[page]();
	}


}