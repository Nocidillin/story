export declare namespace JSX {
	interface Element { }
	interface IntrinsicElements { div: any; }
}
export const entityMap: { [key: string]: string } = { '&': 'amp', '<': 'lt', '>': 'gt', '"': 'quot', '\'': '#39', '/': '#x2F' }

export function escapeHtml(str: string) {
	return String(str).replace(/[&<>"'\/\\]/g, s => `&${entityMap[s]};`)
}

// To keep some consistency with React DOM, lets use a mapper
// https://reactjs.org/docs/dom-elements.html
export function AttributeMapper(val: any): string {
	let comparison: { [key: string]: string } = { "tabIndex": 'tabindex', "className": 'class', "readOnly": 'readonly' }
	return comparison[val] || val;
};

// tslint:disable-next-line:no-default-export
export default function h(
	tag: Function | string,
	attrs?: { [key: string]: any },
	...children: (HTMLElement | string)[]
): HTMLElement {
	attrs = attrs || {}
	const stack: any[] = [...children]

	// Support for components(ish)
	if (typeof tag === 'function') {
		attrs.children = stack
		return tag(attrs)
	}

	const elm = document.createElement(tag)

	// Add attributes
	for (let [name, val] of Object.entries(attrs)) {
		name = escapeHtml(AttributeMapper(name))

		if (val === true) {
			elm.setAttribute(name, name)
		} else if (val !== false && val != null) {
			elm.setAttribute(name, escapeHtml(val))
		} else if (val === false) {
			elm.removeAttribute(name)
		}
	}

	// Append children
	while (stack.length) {
		const child = stack.shift()

		// Is child a leaf?
		if (!Array.isArray(child)) {
			elm.appendChild((child as HTMLElement).nodeType == null ?
				document.createTextNode(child.toString()) :
				child
			)
		} else {
			stack.push(...child)
		}
	}

	return elm
}