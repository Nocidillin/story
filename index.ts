import Universe from './src/Core/Universe'
import Story from './src/Core/Story'

let universe = new Universe();

let story = new Story();
story.Begin();